import React, {Component} from 'react';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import { increase, reset } from '../actions/Counter';

class Counter extends Component {
  increase = () => {
    this.props.increase(this.props.number)
  }

  render(){
    return (
      <Paper style={styles.container}>
        <div style={styles.count}>
          {this.props.number}
        </div>
        <div style={styles.buttons}>
          <RaisedButton
            onClick={this.increase}
            label="Increase"
            primary
          />
          <RaisedButton
            onClick={this.props.reset}
            label="Reset"
            secondary
          />
        </div>
      </Paper>
    )
  }
}

const styles = {
  container: {
    width: 400,
    marginTop: 40,
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: 40,
  },
  count: {
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingTop: 20,
    paddingBottom: 20,
    fontSize: 80,
    textAlign: 'center',
  },
  buttons: {
    display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  },
};

const mapStateToProps = state => ({
  number: state.counter.number,
})

export default connect(mapStateToProps, {
  increase,
  reset,
})(Counter);
// export default Counter;