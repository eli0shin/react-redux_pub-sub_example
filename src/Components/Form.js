import React, {Component} from 'react';
import { connect } from 'react-redux';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from './TextField';
import { submitForm, unmountForm } from '../actions/Form';

class Form extends Component {
  submit = (event) => {
    event.preventDefault();
    this.props.submitForm();
  }

  render(){
    return (
      <Paper
        zDepth={1}
        style={styles.container}
      >
        <form
          onSubmit={this.submit}
          style={styles.form}
        >
          <TextField
            id="email"
            label="Email"
          />
          <TextField
            id="password"
            label="Password"
            type="password"
          />
          <RaisedButton
            type="sumbit"
            label="Submit"
            style={styles.submit}
            primary
            fullWidth
          />
        </form>
      </Paper>
    )
  }

  componentWillUnmount() {
    this.props.unmountForm();
  }
};

const styles = {
  container: {
    maxWidth: 400,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  form: {
    marginTop: 40,
    padding: 40,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
  },
  submit: {
    marginTop: 20,
  },
};

const mapStateToProps = state => ({
  fields: state.formFields,
});

export default connect(mapStateToProps, {
    submitForm,
    unmountForm,
})(Form);