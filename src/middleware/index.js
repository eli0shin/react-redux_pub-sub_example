import counter from './counter';
import form from './form';

export default [
  ...counter,
  ...form,
];