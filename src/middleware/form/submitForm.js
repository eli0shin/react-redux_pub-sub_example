import { SUBMIT_FORM } from '../../actions';

export default ({ dispatch, getState }) => next => action => {
  next(action);

  if (action.type === SUBMIT_FORM) {
    const { formFields } = getState();

    console.log(formFields);
  };
};