import { INCREASE_COUNTER } from '../../actions';
import { increased } from '../../actions/Counter';


export default ({dispatch, getState}) => next => action => {
  next(action);
  if(action.type === INCREASE_COUNTER){
    const currentNumber = getState().counter.number;

    dispatch(increased(currentNumber + 1));
  }
}