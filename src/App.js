import React, { Component } from 'react';
import { Provider } from 'react-redux'
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import reduxStore from './store';
import Counter from './Components/Counter';
import Form from './Components/Form';

class App extends Component {
  render() {
    return (
      <MuiThemeProvider
        muiTheme={getMuiTheme({
          userAgent: false,
        })}
      >
        <Provider store={reduxStore}>
          <React.Fragment>
            <Counter />
            <Form />
          </React.Fragment>
        </Provider>
      </MuiThemeProvider>
    );
  }
}

export default App;