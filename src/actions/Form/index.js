import {
  UPDATE_FORM_FIELD,
  SUBMIT_FORM,
  UNMOUNT_FORM,
} from '../';

export const updateFormField = (id, value) => ({
  type: UPDATE_FORM_FIELD,
  payload: {
    id,
    value,
  },
});

export const submitForm = () => ({
  type: SUBMIT_FORM,
});

export const unmountForm = () => ({
  type: UNMOUNT_FORM,
});