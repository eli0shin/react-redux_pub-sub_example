export const INCREASE_COUNTER = 'INCREASE_COUNTER';
export const RESET_COUNTER = 'RESET_COUNTER';
export const COUNTER_INCREASED = 'COUNTER_INCREASED';

export const UPDATE_FORM_FIELD = 'UPDATE_FORM_FIELD';
export const SUBMIT_FORM = 'SUBMIT_FORM';
export const UNMOUNT_FORM = 'UNMOUNT_FORM';