import { INCREASE_COUNTER, RESET_COUNTER, COUNTER_INCREASED } from '../';

export const increase  = () => ({
  type: INCREASE_COUNTER,
});

export const increased = (number) => ({
  type: COUNTER_INCREASED,
  payload: {
    number,
  },
});

export const reset = () => ({
  type: RESET_COUNTER,
});