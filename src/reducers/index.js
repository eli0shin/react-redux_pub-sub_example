import {combineReducers} from 'redux';
import counter from './counter';
import formFields from './formFields';

export default combineReducers({
  counter,
  formFields,
});