// @flow
import {
  COUNTER_INCREASED,
  RESET_COUNTER,
} from '../../actions/';

const initialState = 0;

export default (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case COUNTER_INCREASED:
      return action.payload.number;
    case RESET_COUNTER:
      return initialState;
    default:
      return state;
  }
};
